﻿app.service('contactsService', function ($http) {
    var baseUrl = 'https://testapi.nzfsg.co.nz/';
    var controller = 'contacts'
    var authHeader = { 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJnaXZlbl9uYW1lIjoidGVzdDIiLCJmYW1pbHlfbmFtZSI6ImxvYW5tYXJrZXQiLCJuaWNrbmFtZSI6InRlc3QyIGxvYW5tYXJrZXQiLCJuYW1lIjoidGVzdDIgbG9hbm1hcmtldCIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci81YThmZTkzMjcxMmY4MDBjNjM5MTFmMGRkZGE4NTIzMz9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnRsLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDE4LTA2LTI1VDE0OjUyOjA4Ljg2OFoiLCJlbWFpbCI6InRlc3R1c2VyMkBsb2FubWFya2V0LmNvLm56IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8vbXljcm0uYXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDU5NjQ5NTU5NGE4ZjkwNWY5MDJlOGVhOCIsImF1ZCI6IlNVWVNUbXdSV3JzVXdCUUw1THNaUmRhOTZTVjhTTTJEIiwiaWF0IjoxNTI5OTM4MzI4LCJleHAiOjE1Mjk5NDAxMjh9.qobZKTqnAmRVu-E3sDrIajQnJn8NeXLbcb6fVSm1cOM' }


    this.GetContacts = function (params) {
        return $http.get(baseUrl + controller + '/FamilyListGet', { params: params, headers: authHeader })
    }
    this.Create = function (contactModel) {
        return $http.post(baseUrl + controller + '/ContactSet', contactModel, { headers: authHeader });
    }
    this.GetFamilyList = function (params) {
        return $http.get(baseUrl + controller + '/ClientInformGet', { params: params, headers: authHeader })
    }
    this.GetTaggedList = function (params) {
        return $http.get(baseUrl + controller + '/TaggedList', { params: params, headers: authHeader })
    }
})