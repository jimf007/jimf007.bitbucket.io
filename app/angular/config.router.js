'use strict';

/**
 * Config for the router
 */
app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires) {

    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;

    // LAZY MODULES

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: jsRequires.modules
    });

    // APPLICATION ROUTES
    // -----------------------------------
    // For any unmatched url, redirect to /app/dashboard
    $urlRouterProvider.otherwise("/app/contacts");
    //
    // Set up the states
    $stateProvider.state('app', {
        url: "/app",
        templateUrl: "/app/pages/app.html",
        resolve: loadSequence('chartjs', 'chart.js', 'chatCtrl'),
    }).state('app.dashboard', {
        url: "/dashboard",
        templateUrl: "/app/assets/frameworks/packet/views/dashboard.html",
        resolve: loadSequence('d3', 'ui.knob', 'countTo', 'dashboardCtrl'),
        title: 'Dashboard',
        ncyBreadcrumb: {
            label: 'Dashboard'
        }
    }).state('app.contacts', {
        url: "/contacts",
        templateUrl: "/app/pages/contacts/contactsIndex.html",
        resolve: loadSequence('ngTable', 'contactsService', 'contactsCtrl', 'contactAddCtrl'),
        title: 'Contacts',
        ncyBreadcrumb: {
            label: 'Contacts'
        },
        controller: 'ContactsCtrl',
        controllerAs: 'vm'
    }).state('app.contactView', {
        url: "/contact/view/:id",
        templateUrl: "/app/pages/contacts/contactView.html",
        resolve: loadSequence('contactsService', 'contactViewCtrl'),
        title: 'Contact Details',
        controller: 'ContactViewCtrl',
        controllerAs: 'vm'
    })
    // Generates a resolve object previously configured in constant.JS_REQUIRES (config.constant.js)
    function loadSequence() {
        var _args = arguments;
        return {
            deps: ['$ocLazyLoad', '$q',
			function ($ocLL, $q) {
			    var promise = $q.when(1);
			    for (var i = 0, len = _args.length; i < len; i++) {
			        promise = promiseThen(_args[i]);
			    }
			    return promise;

			    function promiseThen(_arg) {
			        if (typeof _arg == 'function')
			            return promise.then(_arg);
			        else
			            return promise.then(function () {
			                var nowLoad = requiredData(_arg);
			                if (!nowLoad)
			                    return $.error('Route resolve: Bad resource name [' + _arg + ']');
			                return $ocLL.load(nowLoad);
			            });
			    }

			    function requiredData(name) {
			        if (jsRequires.modules)
			            for (var m in jsRequires.modules)
			                if (jsRequires.modules[m].name && jsRequires.modules[m].name === name)
			                    return jsRequires.modules[m];
			        return jsRequires.scripts && jsRequires.scripts[name];
			    }
			}]
        };
    }
}]);