/** 
  * declare 'packet' module with dependencies
*/
'use strict';
angular.module("MyCRM", [
	'ngAnimate',
	'ngSanitize',
    'ngCookies',
	'ngTouch',
    'ngStorage',
	'ui.router',
	'ui.bootstrap',
	'angularMoment',
	'oc.lazyLoad',
	'swipe',
	'ngBootstrap',
	'truncate',
	'uiSwitch',
	'toaster',
	'ngAside',
	'vAccordion',
	'vButton',
	'oitozero.ngSweetAlert',
	'angular-notification-icons',
	'angular-ladda',
	'angularAwesomeSlider',
	'slickCarousel',
	'cfp.loadingBar',
	'ncy-angular-breadcrumb',
	'duScroll',
    'pascalprecht.translate',
	'FBAngular'
]);