﻿app.controller('ContactViewCtrl', ['$scope', '$stateParams', '$filter', 'contactsService', 'moment',
function ($scope, $stateParams, $filter, contactsService, moment) {
    var vm = this;
    var id = $stateParams.id;
    vm.adultMembers = [];
    vm.childMembers = [];
    vm.mobileNums = [];
    vm.workNums = [];
    vm.homeNums = [];
    vm.personalEmails = [];
    vm.workEmails = [];
    vm.tags = [];
    vm.tagOwners = [];
    vm.nameHeader = '';
    vm.isLoading = true;
    vm.lastUpdate = moment().add(-1, 'days').format('LL');

    function init() {
        vm.isLoading = true;
        getFamilyList();
        getTaggedList();
    }

    function getFamilyList() {
        var params = {
            familyId: id
        }

        return contactsService.GetFamilyList(params).then(function (result) {
            var familyMembers = [];

            angular.copy(result.data, familyMembers);
            angular.copy($filter('filter')(familyMembers, { 'Role': 'Adult' }, true), vm.adultMembers);
            angular.copy($filter('filter')(familyMembers, { 'Role': 'Child' }, true), vm.childMembers);

            generateNameHeader();
            generateNums();
            generateEmails();
            vm.isLoading = false;
        }, function (result) {
            console.log('An error has occurred!');
        });
    }

    function getTaggedList() {
        var params = {
            familyId: id
        }

        return contactsService.GetTaggedList(params).then(function (result) {
            angular.copy(result.data, vm.tags);

            generateTagOwners();
        }, function (result) {
            console.log('An error has occurred!');
        });
    }

    function generateTagOwners() {
        vm.tagOwners = [];
        angular.forEach(vm.tags, function (tag) {
            vm.tagOwners.push(tag.ActivityOwner);
        });
    }

    function generateNameHeader() {
        angular.forEach(vm.adultMembers, function (member, index) {
            vm.nameHeader += member.FullName;

            if (index < vm.adultMembers.length - 1) {
                vm.nameHeader += ' & '
            };
        });
    }

    function generateNums() {
        angular.forEach(vm.adultMembers, function (member) {
            var mobileNum = $filter('filter')(member.Phone, { 'Type': 'Mobile' }, true)[0];
            var homeNum = $filter('filter')(member.Phone, { 'Type': 'Home' }, true)[0];
            var workNum = $filter('filter')(member.Phone, { 'Type': 'Work' }, true)[0];

            if (mobileNum != null) {
                var numObj = {
                    Owner: member.FirstName,
                    Number: mobileNum.Number
                }
                vm.mobileNums.push(numObj);
            }
            if (homeNum != null) {
                var numObj = {
                    Owner: member.FirstName,
                    Number: homeNum.Number
                }
                vm.homeNums.push(numObj);
            }
            if (workNum != null) {
                var numObj = {
                    Owner: member.FirstName,
                    Number: workNum.Number
                }
                vm.workNums.push(numObj);
            }
        });
    }

    function generateEmails() {
        angular.forEach(vm.adultMembers, function (member) {
            var workEmail = $filter('filter')(member.Email, { 'Type': 'WorkEmail' }, true)[0];
            var email = $filter('filter')(member.Email, { 'Type': 'Email' }, true)[0];

            if (workEmail != null) {
                var emailObj = {
                    Owner: member.FirstName,
                    Email: workEmail.EmailAddress
                }
                vm.workEmails.push(emailObj);
            }
            if (email != null) {
                var emailObj = {
                    Owner: member.FirstName,
                    Email: email.EmailAddress
                }
                vm.personalEmails.push(emailObj);
            }
        });
    }

    init();
}]);