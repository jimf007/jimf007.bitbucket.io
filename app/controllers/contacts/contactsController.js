﻿app.controller('ContactsCtrl', ['$scope', '$filter', '$uibModal', 'contactsService', 'NgTableParams', 'moment',
function ($scope, $filter, $uibModal, contactsService, NgTableParams, moment) {
    var vm = this;

    //Variables
    vm.totalResults = 0;
    vm.letterFilter = 'A';
    vm.letterFilters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    vm.searchText = null;
    vm.checkedContacts = [];
    vm.contacts = [];
    vm.displayed = [];
    vm.listDisplayed = [];
    vm.tableParams = new NgTableParams(
        {
            page: 1,
            count: 20,
        },
        {
            counts: [],
            getData: function (params) {
                vm.displayed = vm.contacts.slice((params.page() - 1) * params.count(), params.page() * params.count());
                params.total(vm.contacts.length);
                return vm.displayed;
            }
        }
    );
    vm.lastUpdate = moment().add(-1, 'days').format('LL');

    //Functions
    vm.filterContacts = filterContacts;
    vm.toggleCheckedContacts = toggleCheckedContacts;
    vm.addContact = addContact;

    function addContact() {
        var modalInstance = $uibModal.open({
            templateUrl: 'app/pages/contacts/contactAddModal.html',
            windowClass: 'wide-modal',
            controller: 'ContactAddCtrl',
            controllerAs: 'vm',
            keyboard: false
        })
        .result.then(function (data) {
            filterContacts(false);
        }, function () {

        });
    }

    function toggleCheckedContacts(familyID, isChecked) {
        var idx = vm.checkedContacts.indexOf(familyID);
        if (idx < 0 && isChecked)
        {
            vm.checkedContacts.push(familyID)
        }
        else if (idx >= 0 && !isChecked)
        {
            vm.checkedContacts.splice(idx, 1);
        }
    }

    function getContacts() {
        var params = {
            startWith: vm.letterFilter,
            searchCriteria: vm.searchText,
            byPassFilter: 'true'
        }

        return contactsService.GetContacts(params)
            .then(function (result) {
                angular.copy(result.data.FamilyList, vm.contacts);
                angular.copy(result.data.FamilyList, vm.listDisplayed);
                vm.totalResults = result.data.TotalNumber;
            }, function (result) {
                vm.showError = true;
                console.log('An error has occurred!');
            })
            .finally(function () {
                vm.isLoading = false;
            });
    }

    function filterContacts(isFromSearch) {
        if (!isFromSearch) { //If filter is triggered by change in start letter filter
            vm.searchText = null;
        }

        getContacts().then(function () {
            vm.tableParams.page(1);
            vm.tableParams.reload();
        });
    }

    function init() {
        getContacts().then(function () {
            vm.tableParams.page(1);
            vm.tableParams.reload();
        });
    };

    init();
}]);