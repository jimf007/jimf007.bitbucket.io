﻿app.controller('ContactAddCtrl', ['$scope', '$filter', '$uibModalInstance', 'contactsService', 'moment',
function ($scope, $filter, $uibModalInstance, contactsService, moment) {
    var vm = this;

    //Variables
    vm.isSmoker = null;
    vm.roles = [null, 'Adult', 'Child', 'Other', 'Guarantor', 'Policy Owner'];
    vm.titles = [null, 'Mr', 'Mrs', 'Ms', 'Miss', 'Dr'];
    vm.classes = [
        null,
        {
            id: 1,
            name: 'Class 1',
        },
        {
            id: 2,
            name: 'Class 2',
        },
        {
            id: 3,
            name: 'Class 3',
        },
        {
            id: 4,
            name: 'Class 4',
        },
        {
            id: 5,
            name: 'Class 5',
        }
    ]
    vm.contactModel = {
        LastName: '',
        FirstName: '',
        MiddleName: '',
        PreferredName: '',
        Title: null,
        Gender: null,
        Role: null,
        DateOfBirth: '',
        Phone: [
            {
                Type: 'Home',
                Number: ''
            },
            {
                Type: 'Business',
                Number: ''
            },
            {
                Type: 'Mobile',
                Number: ''
            }
        ],
        BestTimeToCall: '',
        Employment: [
            {
                Occupation: '',
                OccupationClass: 0,
                Employer: ''
            }
        ],
        SmokerStatusBoolValue: false,
        CompanyName: '',
        DOBReminder: false,
        Deceased: false,
        Email: [
            {
                Type: 'Email',
                EmailAddress: ''
            },
            {
                Type: 'Work',
                EmailAddress: ''
            }
        ],
        Notes: ''
    }
    vm.contactNumbers = {
        Business: '',
        Home: '',
        Mobile: ''
    };
    vm.timeToCall = {
        Hour: null,
        Min: null,
        AMPM: 'AM'
    };
    vm.emailAddress = '';

    //Functions
    vm.save = save;
    vm.toggleTime = toggleTime;

    function save(isValid) {
        if (isValid) {
            var model = [constructModelForAPI()];
            
            contactsService.Create(model).then(function (result) {
                $uibModalInstance.close();
            }, function (result) {
                console.log('An error has occurred');
            });
        }
    }

    function toggleTime() {
        if (vm.timeToCall.AMPM == 'AM') {
            vm.timeToCall.AMPM = 'PM';
        }
        else {
            vm.timeToCall.AMPM = 'AM';
        }
    }

    function constructModelForAPI() {
        //Construct time to call
        if (vm.timeToCall.Hour != null || vm.timeToCall.Min != null) {
            var hrs = vm.timeToCall.Hour == null ? '00' : vm.timeToCall.Hour;
            var mins = vm.timeToCall.Min == null ? '00' : vm.timeToCall.Min.toString().length < 2 ? '0' + vm.timeToCall.Min : vm.timeToCall.Min;
            var timeToCall = hrs + ':' + mins + ' ' + vm.timeToCall.AMPM;

            vm.contactModel.BestTimeToCall = timeToCall;
        }

        //Construct contact numbers
        var contactNumbers = [];
        angular.forEach(vm.contactNumbers, function (value, key) {
            if (value != null && value.length > 0) {
                var phone =
                {
                    Type: key,
                    Number: value
                };

                contactNumbers.push(phone);
            };
        });
        vm.contactModel.Phone = contactNumbers;

        //Emails
        var contactNumbers = [];
        vm.contactModel.Email = [{
            Type: 'Email',
            EmailAddress: vm.emailAddress
        }];

        vm.contactModel.SmokerStatusBoolValue = vm.isSmoker != null ? (vm.isSmoker == 'True' ? true : false) : null;

        return vm.contactModel;
    }

}]);